# Wiki 

## PSR-7 (Norme)

La [PSR-7](http://www.php-fig.org/psr/psr-7/) définie les pratiques en vigueurs ayant trait à la manipulation des messages **HTTP** en PHP. Un message HTTP peut être une **Requete** ou une **Réponse**


### [Response](PSR-7/Response.md)


## PSR-7 (Implémentation dans BoondManager)

### Guzzle

[Guzzle](docs.guzzlephp.org/) est une implémentation de la norme PSR-7 orientée client. (en gros, quand tu veux utiliser CUrl, à la place tu utilises Guzzle)

```php
<?php
$client = new \GuzzleHttp\Client();
$res = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
echo $res->getStatusCode();
// 200
echo $res->getHeaderLine('content-type');
// 'application/json; charset=utf8'
echo $res->getBody();
// '{"id": 1420053, "name": "guzzle", ...}'
```


### ResponseSenderTrait

Répondant au doux FQN de `BoondManager\Lib\HTTP\Psr7\ResponseSenderTrait`, c'est une implémentation de PSR-7 serveur.

`sendFile(string|ressource|generator $file)` permet d'envoyer un fichier au navigateur.
